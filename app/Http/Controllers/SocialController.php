<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\User;

class SocialController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    public function callback($provider)
    {
        $getInfo = Socialite::driver($provider)->user();
        if (!$getInfo) {
            echo 'Facebook login Error!';
            exit();
        }
        $user = $this->createUser($getInfo,$provider);
        auth()->login($user);
        return redirect('welcome');
    }

    function createUser($getInfo,$provider){
        $user = User::where('provider_id', $getInfo->id)->first();
        if (!$user) {
            $personals = explode(' ', $getInfo->name, 2);
            $name = $getInfo->name;
            $surname = $getInfo->name;

            if (isset($personals[0])) {
                $name = $personals[0];
            }
            if (isset($personals[1])) {
                $surname = $personals[1];
            }

            $user = User::create([
                'name'     => $name,
                'surname'  => $surname,
                'email'    => $getInfo->email,
                'provider' => $provider,
                'provider_id' => $getInfo->id
            ]);
        }
        return $user;
    }
}
