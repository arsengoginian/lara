<?php

namespace App\Http\Controllers;

use App\User;
use App\WelcomeMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('admin.index', compact('users'));
    }

    public function settings()
    {
        $message = WelcomeMessage::first();

        if (!$message) {
            $message = new WelcomeMessage();
            $message->text = ' Dear [NAME] [SURNAME], thank you for your registration!';
            $message->save();
        }

        return view('admin.settings', compact('message'));
    }

    public function updateSettings(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'text' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput(Input::all());
        }

        $message = WelcomeMessage::first();

        $message->text = $request->text;
        $message->save();

        return back();
    }

    public function loginForm()
    {
        Auth::logout();

        return view('admin.login');
    }

    public function login(Request $request)
    {

    }
}
