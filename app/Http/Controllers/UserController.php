<?php

namespace App\Http\Controllers;

use App\User;
use App\WelcomeMessage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'city' => ['string', 'max:255', 'nullable'],
            'birthdate' => ['date', 'nullable'],
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput(Input::all());
        }

        $data = $request->all();

        $user = User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'city' => $data['city'],
            'birthdate' => Carbon::parse($data['birthdate']),
        ]);

        Auth::login($user);
        return redirect('welcome');
    }

    public function welcome()
    {
        $user = Auth::user();
        $message = WelcomeMessage::first();

        $text = $message->text;
        $text= str_replace('[NAME]', $user->name, $text);
        $text= str_replace('[SURNAME]', $user->surname, $text);
        return view('welcome', compact('text'));
    }
}
