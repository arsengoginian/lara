@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        <form action="{{ route('admin.settings') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="text">Welcome Message (can use [NAME] and [SURNAME] as shortcodes)</label>
                                <textarea class="form-control" name="text" id="text" cols="30" rows="10">{{ $message->text }}</textarea>
                            </div>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection