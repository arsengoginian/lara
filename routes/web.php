<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::post('/register', 'UserController@register')->name('register');
Route::get('/welcome', 'UserController@welcome')->name('welcome')->middleware('auth');

Route::get('/auth/redirect/{provider}', 'SocialController@redirect');
Route::get('/callback/{provider}', 'SocialController@callback');


Route::get('/admin', 'AdminController@index')->name('admin')->middleware('auth:admin');
Route::get('/admin/login', 'AdminController@loginForm')->name('admin.login');
Route::post('/admin/logout', 'Auth\LoginController@logout')->name('admin.logout')->middleware('auth:admin');
Route::post('/admin/login', 'Auth\LoginController@login');
Route::get('/admin/settings', 'AdminController@settings')->name('admin.settings')->middleware('auth:admin');
Route::post('/admin/settings', 'AdminController@updateSettings')->middleware('auth:admin');


Route::get('/policy', function () {
    return view('policy');
})->name('policy');
